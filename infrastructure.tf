variable "region" {
  description = "AWS region"
}

variable "function_bucket" {
  description = "the bucket containing the artefact"
}

variable "deployment_stage" {
  description = "the lambda ZIP file name in the bucket"
}

provider "aws" {
  region = var.region
}

locals {
  function_deployment_name =  "helloaws-${var.deployment_stage}"
  function_object_key = "helloaws-${var.deployment_stage}.zip"
}

data "aws_s3_bucket_object" "function_package" {
  bucket = var.function_bucket
  key = local.function_object_key
}

resource "aws_lambda_function" "function_lambda" {
  function_name = local.function_deployment_name
  s3_bucket = var.function_bucket
  s3_key = local.function_object_key
  s3_object_version = data.aws_s3_bucket_object.function_package.version_id
  handler = "helloaws"
  runtime = "go1.x"
  role = aws_iam_role.function_exec.arn

  environment {
    variables = {
      stage = var.deployment_stage
    }
  }
}

resource "aws_iam_role" "function_exec" {
  name = "${local.function_deployment_name}_lambda"
  assume_role_policy = file("${path.module}/iam_role_lambda.json")
}

resource "aws_iam_role_policy" "function_acl" {
  name = "${local.function_deployment_name}_lambda_policy"
  role = aws_iam_role.function_exec.id
  policy = file("${path.module}/iam_policy_lambda.json")
}

resource "aws_api_gateway_rest_api" "api_root" {
  name = local.function_deployment_name
  description = "An API that greets you"
}

resource "aws_api_gateway_resource" "api_greet" {
  parent_id = aws_api_gateway_rest_api.api_root.root_resource_id
  path_part = "greet"
  rest_api_id = aws_api_gateway_rest_api.api_root.id
}

resource "aws_api_gateway_method" "api_get_greet" {
  rest_api_id = aws_api_gateway_rest_api.api_root.id
  resource_id = aws_api_gateway_resource.api_greet.id
  http_method = "GET"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "call_lambda" {
  type = "AWS_PROXY"
  integration_http_method = "POST"
  uri = aws_lambda_function.function_lambda.invoke_arn
  http_method = aws_api_gateway_method.api_get_greet.http_method
  resource_id = aws_api_gateway_resource.api_greet.id
  rest_api_id = aws_api_gateway_rest_api.api_root.id
}

resource "aws_api_gateway_deployment" "current" {
  rest_api_id = aws_api_gateway_rest_api.api_root.id
  depends_on = [aws_api_gateway_integration.call_lambda]
  stage_name = var.deployment_stage
}

resource "aws_lambda_permission" "gateway" {
  action = "lambda:InvokeFunction"
  function_name = aws_lambda_function.function_lambda.function_name
  principal = "apigateway.amazonaws.com"
  source_arn = "${aws_api_gateway_rest_api.api_root.execution_arn}/*/*"
}

output "greet_url" {
  value = aws_api_gateway_deployment.current.invoke_url
}
