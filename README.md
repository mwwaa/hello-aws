# Hello AWS

Hello AWS is your basic Hello World ! program, implemented with Go, hosted on AWS Lambda and deployed with Terraform. The project's value lies in its Terraform configuration, CI pipeline and the article describing the key decisions in making this work.

## Setup

If you want to deploy your own *Hello AWS* service, the easiest way is to clone this project and use Terraform CLI (run Terraform locally). You will need :

* An [AWS](https://aws.amazon.com) account
* An AWS IAM access key with sufficient authorizations to create a Lambda and an API Gateway
* The [Go](https://golang.org) compiler toolchain
* [Terraform CLI](https://terraform.io)
* A S3 client such as [AWS CLI](https://aws.amazon.com/cli/)
* A S3 bucket
* Some utility software to zip files

**Warning : Running the following procedure, you will use AWS services, which might cost you some money. AWS might bill you.**

1. Setup AWS environment variables in your shell (it would be better to set them temporarily so that your whole system does not have access to your AWS account)
    * `AWS_ACCESS_KEY_ID`
    * `AWS_SECRET_ACCESS_KEY`
2. Clone this repository using `git clone https://gitlab.com/mwwaa/hello-aws.git`
3. From inside the cloned repository, build `GOOS=linux go build -o helloaws gitlab.com/mwwaa/hello-aws`
4. Then test `GOOS=linux go test gitlab.com/mwwaa/hello-aws`
5. Zip `helloaws` into `helloaws.zip` using for example `zip helloaws.zip helloaws`
6. Using your S3 client, send the zip file to a S3 bucket of your choice, for example `aws s3 cp helloaws.zip s3://your.functions.bucket/helloaws-master.zip`
7. Run Terraform plan to see what's gonna happen in your AWS account : `terraform plan -var 'region=YOUR_AWS_REGION' -var 'function_bucket=your.functions.bucket' -var 'deployment_stage=master'`
8. Run Terraform to apply the plan `terraform apply -var 'region=YOUR_AWS_REGION' -var 'function_bucket=your.functions.bucket' -var 'deployment_stage=master'`
9. Test it using Terraform's output variable (which should be an API Gateway HTTPS URL) + `/master/greet`
10. You can tear it down using `terraform destroy -var 'region=YOUR_AWS_REGION' -var 'function_bucket=your.functions.bucket' -var 'deployment_stage=master'`

With such a list of dependencies and steps, why did'nt I created a Dockerfile to handle build and deployment ? Well, because I specifically wrote the Gitlab CI/CD to handle those tasks for me (and the steps from above are pretty much the contents of the CI file). I then stopped deploying from my computer. As for testing ... I use automatic tests =)

## Acknowledgment

I built this thanks to :

* [Terraform's own tutorial using Node.js](https://learn.hashicorp.com/terraform/aws/lambda-api-gateway)
* [The article that does exactly what I wanted](https://rogerwelin.github.io/aws/serverless/terraform/lambda/2019/03/18/build-a-serverless-website-from-scratch-with-lambda-and-terraform.html)
* [How to add Terraform commands in the CI](https://medium.com/@timhberry/terraform-pipelines-in-gitlab-415b9d842596)
* [How to push artifacts from Gitlab CI to S3](https://rpadovani.com/aws-s3-gitlab)
