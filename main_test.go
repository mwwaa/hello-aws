package main

import (
	"gitlab.com/mwwaa/tagol.git"
	"net/http"
	"os"
	"testing"
)

func TestHttpOk(t *testing.T) {
	tagol.Invoking(handleRequest).
		Should(t).
		ReturnNoError().
		ReturnStatus(http.StatusOK)
}

func TestDefaultMessage(t *testing.T) {
	tagol.Invoking(handleRequest).
		Should(t).
		ReturnNoError().
		ReturnBodyAs(new(greetings), &greetings{"Hello, dear ! I'm so happy too 🙂\n\nWe're currently in  🤖"})
}

func TestNamedMessage(t *testing.T) {
	tagol.Invoking(handleRequest).
		WithQuery("firstName", "Maxime").
		Should(t).
		ReturnNoError().
		ReturnBodyAs(new(greetings), &greetings{"Hello, Maxime ! I'm so happy too 🙂\n\nWe're currently in  🤖"})
}

func TestMoodMessage(t *testing.T) {
	tagol.Invoking(handleRequest).
		WithQuery("mood", "hungry").
		Should(t).
		ReturnNoError().
		ReturnBodyAs(new(greetings), &greetings{"Hello, dear ! I'm so hungry too 🙂\n\nWe're currently in  🤖"})
}

func TestNamedMoodMessage(t *testing.T) {
	tagol.Invoking(handleRequest).
		WithQuery("firstName", "Maxime").
		WithQuery("mood", "hungry").
		Should(t).
		ReturnNoError().
		ReturnBodyAs(new(greetings), &greetings{"Hello, Maxime ! I'm so hungry too 🙂\n\nWe're currently in  🤖"})
}

func TestContentType(t *testing.T) {
	tagol.Invoking(handleRequest).
		Should(t).
		ReturnNoError().
		ReturnHeader("Content-Type", "text/plain; charset=utf-8")
}

func TestStageMessage(t *testing.T) {
	if err := os.Setenv("stage", "test"); err != nil {
		t.FailNow()
	}
	tagol.Invoking(handleRequest).
		Should(t).
		ReturnNoError().
		ReturnBodyAs(new(greetings), &greetings{"Hello, dear ! I'm so happy too 🙂\n\nWe're currently in test 🤖"})
}
