package main

import (
	"encoding/json"
	"fmt"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"net/http"
	"os"
)

type greetings struct {
	Greeting string
}

func main() {
	fmt.Printf("Hello, CloudWatch ! %v starting", os.Getenv("stage"))
	lambda.Start(handleRequest)
}

func handleRequest(req events.APIGatewayProxyRequest) (resp events.APIGatewayProxyResponse, err error) {
	fmt.Println("Processing event.")

	// Apparently we could also use req.RequestContext.Stage,
	// but the point is to see how to pass values that are more useful than the "stage", if needed
	stage := os.Getenv("stage")

	firstName := firstNameOrDefault(req.QueryStringParameters)
	mood := moodOrDefault(req.QueryStringParameters)
	greeting := fmt.Sprintf("Hello, %v ! I'm so %v too 🙂\n\nWe're currently in %v 🤖", firstName, mood, stage)

	raw, err := json.Marshal(greetings{greeting})

	resp.StatusCode = http.StatusOK
	resp.Body = string(raw)
	resp.Headers = map[string]string{"Content-Type": "text/plain; charset=utf-8"}

	return
}

func firstNameOrDefault(query map[string]string) string {
	return queryParameterOrDefault(query, "firstName", "dear")
}

func moodOrDefault(query map[string]string) string {
	return queryParameterOrDefault(query, "mood", "happy")
}

func queryParameterOrDefault(query map[string]string, key string, def string) string {
	value, exists := query[key]

	if exists {
		return value
	}

	return def
}
