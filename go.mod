module gitlab.com/mwwaa/hello-aws

go 1.13

require (
	github.com/aws/aws-lambda-go v1.14.1
	github.com/urfave/cli v1.22.1 // indirect
	gitlab.com/mwwaa/tagol.git v1.0.1
)
